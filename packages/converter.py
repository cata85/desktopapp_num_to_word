basic_nums = {1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
              10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
              17: 'seventeen', 18: 'eighteen', 19: 'nineteen', 20: 'twenty', 30: 'thirty', 40: 'forty', 50: 'fifty',
              60: 'sixty', 70: 'seventy', 80: 'eighty', 90: 'ninety', 0: ''
              }
generals = ['', 'thousand', 'million', 'billion']


# iterates through numbers by sets of 3 digits at a time
def num_to_string(num):
    num_length = num_length_checker(str(num))
    str_num = ''

    if num is 0:
        str_num = 'zero'
    else:
        for i in range(num_length):
            temp = int(str(num)[:len(str(num)) % 3]) if len(str(num)) % 3 is not 0 else int(str(num)[:3])
            third = temp // 100
            if third is not 0:
                str_num += basic_nums.get(third) + 'hundred '
                if temp % 100 is 0:
                    num = int(str(num)[2:])
                    temp = 0
                else:
                    num = int(str(num)[1:])
                    temp = int(str(temp)[1:])
            second = temp // 10
            if second > 1:
                str_num += basic_nums.get(second * 10) + basic_nums.get(temp % 10) + ' '
                try:
                    num = int(str(num)[2:])
                except ValueError:
                    num = 0
            else:
                str_num += basic_nums.get(temp)
            str_num += generals[num_length - i - 1] + ' '
            try:
                num = int(str(num)[len(str(num)) % 3:])
            except ValueError:
                num = 0
    return str_num


# gives how many times a set of 3 digits occur for how many times the loop should run
def num_length_checker(num):
    return int(len(num) // 4) + 1
