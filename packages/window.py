from PyQt5 import QtWidgets
from packages import converter


class Window(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.text_label = QtWidgets.QLabel('Enter a number zero to a billion')
        self.text_line = QtWidgets.QLineEdit()
        self.enter_text = QtWidgets.QPushButton('Enter')

        v_box = QtWidgets.QVBoxLayout()
        v_box.addStretch()
        v_box.addWidget(self.text_label)
        v_box.addStretch()
        v_box.addWidget(self.text_line)
        v_box.addStretch()
        v_box.addWidget(self.enter_text)
        self.setLayout(v_box)
        self.setWindowTitle('Number to Word')

        self.enter_text.clicked.connect(self.btn_clicked)

    def btn_clicked(self):
        try:
            self.text_label.setText(converter.num_to_string(int(self.text_line.text())))
        except ValueError:
            self.text_label.setText('Enter a valid number')
