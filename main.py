import sys
from packages import window
from PyQt5 import QtWidgets


app = QtWidgets.QApplication(sys.argv)

main_window = window.Window()

main_window.show()


sys.exit(app.exec_())
